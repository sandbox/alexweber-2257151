DRealty for Drupal 8
===

Drupal RETS Real Estate Framework integration

This is very much in development and might or might not work at any given moment.

Roadmap
===

Milestone 1
---
* Fix Listing Type config entity
* Fix Listing content entity
* Figure out suporting D7 module classes (drealtyConnection, drealtyMetaData, etc) and port as appropriate
* Classes/Resources/Mappings/etc as config entities?
* Use PHPRETS via Composer's autoloader

Milestone 2
---
* Implement RETS Refresh for individual listings
* Views integration
* Devel integration
* Token integration
* Actions plugin
* Conditions plugin
* Entity Reference plugin
* Search integration

Milestone 3
---
* Port Daemon/Drush integration
* Leverage States API & do cron magic to auto update feeds?

Milestone 4
---
* Get a better toolbar icon?
* Convert CSS to SASS?
